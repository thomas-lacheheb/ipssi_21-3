start:
	docker-compose up -d

stop:
	docker-compose down

build:
	docker-compose build

# add_composer:
# 	docker-compose run php composer install

clean:
	docker system prune -a -f

up: build start